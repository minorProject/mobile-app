import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyCOJNP6HBsYMLgLrfoLypTn70DTTCHEvLE",
    authDomain: "travelove-40f16.firebaseapp.com",
    databaseURL: "https://travelove-40f16.firebaseio.com",
    projectId: "travelove-40f16",
    storageBucket: "travelove-40f16.appspot.com",
    messagingSenderId: "616910913863"
  };
  export const firebaseRef = firebase.initializeApp(config);