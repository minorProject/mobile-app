// @flow
import * as React from "react";
import Chatbot from "../../stories/screens/Chatbot";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ChatbotContainer extends React.Component<Props, State> {
	render() {
		return <Chatbot navigation={this.props.navigation} />;
	}
}
