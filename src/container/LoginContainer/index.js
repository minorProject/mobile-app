// @flow
import * as React from "react";
import { Item, Input, Icon, Toast, Form,Container,Label,Button} from "native-base";
// import { Field, reduxForm } from "redux-form";
import Login from "../../stories/screens/Login";
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { firebaseRef } from "../../../services/firebase";

// const required = value => (value ? undefined : "Required");
// const maxLength = max => value =>
//   value && value.length > max ? `Must be ${max} characters or less` : undefined;
// const maxLength15 = maxLength(15);
// const minLength = min => value =>
//   value && value.length < min ? `Must be ${min} characters or more` : undefined;
// const minLength8 = minLength(8);
// const email = value =>
//   value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
//     ? "Invalid email address"
//     : undefined;
// const alphaNumeric = value =>
//   value && /[^a-zA-Z0-9 ]/i.test(value)
//     ? "Only alphanumeric characters"
//     : undefined;

export interface Props {
  navigation: any;
}
export interface State {}

// class LoginForm extends React.Component<Props, State> {
//   textInput: any;

//   renderInput({ input, label, type, meta: { touched, error, warning } }) {
//     return (
//       <Item error={error && touched}>
//         <Icon active name={input.name === "email" ? "person" : "unlock"} />
//         <Input
//           ref={c => (this.textInput = c)}
//           placeholder={input.name === "email" ? "Email" : "Password"}
//           secureTextEntry={input.name === "password" ? true : false}
//           {...input}
//         />
//       </Item>
//     );
//   }

//   login() {
//     if (this.props.valid) {
//       this.props.navigation.navigate("Drawer");
//     } else {
    //   Toast.show({
    //     text: "Enter Valid Username & password!",
    //     duration: 2000,
    //     position: "top",
    //     textStyle: { textAlign: "center" }
    //   });
//     }
//   }

//   render() {
//     const form = (
//       <Form>
//         <Field
//           name="email"
//           component={this.renderInput}
//           validate={[email, required]}
//         />
//         <Field
//           name="password"
//           component={this.renderInput}
//           validate={[alphaNumeric, minLength8, maxLength15, required]}
//         />
//       </Form>
//     );
//     return (
    //   <Login
    //     navigation={this.props.navigation}
    //     loginForm={form}
    //     onLogin={() => this.login()}
    //   />
//     );
//   }
// }
// const LoginContainer = reduxForm({
//   form: "login"
// })(LoginForm);
// export default LoginContainer;

// --------------------------------------------------------------------

class LoginContainer extends React.Component<Props, State> {
  constructor(props) {
      super(props);
      this.state = {
        password: "",
        email: "",
      };
    }
  

  login = (email,password) =>{
      try{
          if(this.state.email!="" && this.state.password!=""){
              firebaseRef.auth().signInWithEmailAndPassword(email,password).then(function (user){
                  console.log("logged in");
                  if(user != undefined){
                      // Actions.home();
                      this.props.navigation.navigate("Drawer");
                  }
              })
          }
          else{
            Toast.show({
                text: "Enter Valid Username & password!",
                duration: 2000,
                position: "top",
                textStyle: { textAlign: "center" }
              });
          }

      }catch(error){
          console.log(error);
      }
  }

  render (){
      const form = (
        <Form>
            <Item floatingLabel>
                <Label>Email</Label>
                <Input autoCorrect={false} autoCapitalize='none' onChangeText={(email) => this.setState({email})}></Input>
            </Item>
            <Item floatingLabel>
                <Label>Password</Label>
                <Input secureTextEntry={true} autoCorrect={false} onChangeText={(password) => this.setState({password})}></Input>
            </Item>
            {/* <Button style={{marginTop:20}} block rounded onPress={() => this.login(this.state.email,this.state.password)}>
                <Text>Log In</Text>
            </Button>
            <Button style={{marginTop:20}} block rounded onPress={() => Actions.singup()}>
                <Text>Create an Acoount</Text>
            </Button> */}
        </Form>
      );
      return(
        <Login
        navigation={this.props.navigation.navigate("Drawer")}
        loginForm={form}
        onLogin={() => this.login(this.state.email,this.state.password)}
      />
  );
}
}

// const styles = StyleSheet.create({
// container: {
//   flex: 1,
//   justifyContent: 'center',
//   backgroundColor: 'white',
// },
// welcome: {
//   fontSize: 20,
//   textAlign: 'center',
//   margin: 10,
//   color: 'black',
// },
// });

export default LoginContainer;