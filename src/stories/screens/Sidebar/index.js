import * as React from "react";
import { Text, Container, List, ListItem, Content } from "native-base";
import { NavigationActions } from "react-navigation";
import { firebaseRef } from "../../../../services/firebase";

const routes = [
	{
		route: "Home",
		caption: "Home",
	},
	{
		route: "Chatbot",
		caption: "Chatbot",
	},

	{
		route: "BlankPage",
		caption: "Blank Page",
	},
	{
		route: "Login",
		caption: "Logout",
	},
];

export interface Props {
	navigation: any,
}
export interface State {}

const resetAction = NavigationActions.reset({
	index: 0,
	actions: [NavigationActions.navigate({ routeName: "Login" })],
});

const logout=()=>{
    firebaseRef.auth().signOut()
  .then(function() {
    console.log("logged out");
	// Actions.login();
	this.props.navigation.navigate("Login")
  })
  .catch(function(error) {
    console.log(error);
  });
}

export default class Sidebar extends React.Component<Props, State> {
	render() {
		return (
			<Container>
				<Content>
					<List
						style={{ marginTop: 40 }}
						dataArray={routes}
						renderRow={data => {
							return (
								<ListItem
									button
									onPress={() => {
										data.route === "Login"
											? this.logout()
											: this.props.navigation.navigate(data.route);
									}}
								>
									<Text>{data.caption}</Text>
								</ListItem>
							);
						}}
					/>
				</Content>
			</Container>
		);
	}
}
